using UnityEngine;

public enum ElevatorState
{
    Moving,
    Blocked,
    CanMove
}
[CreateAssetMenu(fileName ="ElevatorState")]
public class ElevatorStateSO : ScriptableObject
{
    public ElevatorState CurrentElevatorState => _currentElevatorState;

    [SerializeField] private ElevatorState _currentElevatorState = default;

    private void OnEnable()
    {
        _currentElevatorState = ElevatorState.CanMove;
    }
    public void UpdateElevatorState(ElevatorState newElevatorState)
    {
        if(newElevatorState == CurrentElevatorState)
        {
            return;
        }
        _currentElevatorState = newElevatorState;
    }
}
