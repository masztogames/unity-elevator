using UnityEngine;

public class InteractionHandler : MonoBehaviour
{
    [SerializeField] private InputReader _inputReader = default;

    private InteractionDetector _interactionDetector;

    private GameObject _currentTarget;
    private IInteractable _currentInteractableTarget;

    private bool _isObjectInteractable = false;

    [SerializeField] private GameObject _interactionPointer;

    private void OnEnable()
    {
        _inputReader.InteractionButtonEvent += OnInteractionButton;
    }
    private void OnDisable()
    {
        _inputReader.InteractionButtonEvent -= OnInteractionButton;
    }
    private void OnInteractionButton()
    {
        if (_isObjectInteractable)
        {
            _currentInteractableTarget?.OnInteract();
        }
    }
    private void Start()
    {
        _interactionDetector = GetComponent<InteractionDetector>();
        Cursor.lockState = CursorLockMode.Confined;

    }
    void Update()
    {
        if (_interactionDetector.GetCurrentTarget() == null)
        {
            _isObjectInteractable = false;
            _currentInteractableTarget = null;
            return;
        }

        _currentTarget = _interactionDetector.GetCurrentTarget();

        if (_currentTarget.GetComponent<IInteractable>() == null)
        {
            _isObjectInteractable = false;
            _interactionPointer.SetActive(false);

            return;
        }
        _currentInteractableTarget = _currentTarget.GetComponent<IInteractable>();

        _interactionPointer.SetActive(true);

        _isObjectInteractable = true;

    }
}
