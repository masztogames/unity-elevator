using UnityEngine;

public class InteractionDetector : MonoBehaviour
{
    [SerializeField] private float _rayRange = 4f;
    [SerializeField] private Transform _rayCastPoint;

    private GameObject _currentTarget;
    void Update()
    {
        DoRaycast();
    }
    public GameObject GetCurrentTarget()
    {
        return _currentTarget;
    }
    private void DoRaycast()
    {
        RaycastHit hit;
        if (Physics.Raycast(_rayCastPoint.transform.position, _rayCastPoint.transform.forward, out hit, _rayRange))
        {
            Debug.DrawRay(_rayCastPoint.transform.position, _rayCastPoint.transform.forward * _rayRange, Color.green);

            GameObject detectedGameObject = hit.collider.gameObject;
            if (detectedGameObject == null)
            {
                _currentTarget = null;
                return;
            }
            if (detectedGameObject != null)
            {
                _currentTarget = detectedGameObject;
            }
        }
        else
        {
            _currentTarget = null;

        }
    }
}
