public interface IInteractable
{
    float MaxRange { get; }
    void OnInteract();
}
