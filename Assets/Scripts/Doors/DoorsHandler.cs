using UnityEngine;

public class DoorsHandler : MonoBehaviour
{
    public Doors[] Doors;

    public IntVariable FloorNumber;

    public void OpenDoorsOnFloor(int floor)
    {
        Doors[floor].OpenDoors();
    }
    public void CloseDoorsOnFloor(int floor)
    {
        if (Doors[floor].DoorsAreOpen)
        {
            Doors[floor].CloseDoors();
            Doors[floor].EmergencyOpening = false;
        }
    }
}
