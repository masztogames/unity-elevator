using System.Collections;
using UnityEngine;

public class Doors : MonoBehaviour
{
    private Animator _animator;

    [SerializeField] private ElevatorStateSO _elevatorState;

    [SerializeField] private AudioClip _doorOpenSound;
    [SerializeField] private AudioClip _doorCloseSound;

    [SerializeField] private float _timeToCloseDoor;

    private AudioSource _audioSource;

    public bool EmergencyOpening;
    public bool DoorsAreOpen;

    void Start()
    {
        _animator = GetComponent<Animator>();
        _audioSource = GetComponent<AudioSource>();
    }
    public void OpenDoors()
    {
        _elevatorState.UpdateElevatorState(ElevatorState.Blocked);
        Debug.Log("Opening the door");
        _audioSource.PlayOneShot(_doorOpenSound);
        DoorsAreOpen = true;
        _animator.Play("Door_Open_Animation");
        StartCoroutine(AutomaticClose());
    }
    IEnumerator AutomaticClose()
    {
        yield return new WaitForSeconds(_timeToCloseDoor);
        if (DoorsAreOpen)
        {
            CloseDoors();
        }
    }
    public void CloseDoors()
    {
        if (EmergencyOpening)
        {
            Debug.Log("Can't close the doors");
            _audioSource.PlayOneShot(_doorCloseSound);

            return;
        }
        Debug.Log("Closing the Door");
        DoorsAreOpen = false;
        _audioSource.PlayOneShot(_doorCloseSound);
        _animator.Play("Door_Close_Animation");
    }

    public void ReleseElevator()
    {
        _elevatorState.UpdateElevatorState(ElevatorState.CanMove);
    }
    private void OnTriggerStay(Collider other)
    {
        if (!DoorsAreOpen)
        {
            return;
        }
        if (other.transform.tag == "Player")
        {
            EmergencyOpening = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (!DoorsAreOpen)
        {
            return;
        }
        if (other.transform.tag == "Player")
        {
            EmergencyOpening = false;
            Debug.Log("Trying to close the door again");
            CloseDoors();
        }

    }
}
