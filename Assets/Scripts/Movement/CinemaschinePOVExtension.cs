using UnityEngine;
using UnityEngine.InputSystem;
using Cinemachine;

public class CinemaschinePOVExtension : CinemachineExtension
{
    [SerializeField] private float _clampAngle = 80f;
    [SerializeField] private float _horizontalSpeed = 10f;
    [SerializeField] private float _verticalSpeed = 10f;
    [SerializeField] private InputReader _inputReader = default;
    private Vector3 _startingRotation;
    private Vector2 _deltaInput;

    protected override void OnEnable()
    {
        base.OnEnable();
        _inputReader.CameraMoveEvent += OnLook;
    }
    private void OnDisable()
    {
        _inputReader.CameraMoveEvent -= OnLook;
    }

    protected override void Awake()
    {
        base.Awake();
        if (_startingRotation == null)
            _startingRotation = transform.localRotation.eulerAngles;
    }
    protected override void PostPipelineStageCallback(CinemachineVirtualCameraBase vcam, CinemachineCore.Stage stage, ref CameraState state, float deltaTime)
    {
        if (vcam.Follow)
        {
            if (stage == CinemachineCore.Stage.Aim)
            {
                _startingRotation.x += _deltaInput.x * _verticalSpeed * Time.deltaTime;
                _startingRotation.y += _deltaInput.y * _horizontalSpeed * Time.deltaTime;
                _startingRotation.y = Mathf.Clamp(_startingRotation.y, -_clampAngle, _clampAngle);
                state.RawOrientation = Quaternion.Euler(-_startingRotation.y, _startingRotation.x, 0f);
            }
        }
    }

    private void OnLook(Vector2 deltaInput)
    {
        _deltaInput = deltaInput;
    }
}
