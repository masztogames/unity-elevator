using UnityEngine;

public class FirstPersonController : MonoBehaviour
{
    [SerializeField]
    private Camera _camera;
    [SerializeField]
    private CharacterController _controller;
    [SerializeField]
    private Vector3 _playerVelocity;
    [SerializeField]
    private bool _groundedPlayer;
    [SerializeField]
    private float _playerSpeed = 2.0f;
    [SerializeField]
    private float _gravityValue = -9.81f;
    [SerializeField]
    private InputReader _inputReader = default;
    private Vector2 _movement;
    private Transform _cameraTransform;
    private void Start()
    {
        _cameraTransform = _camera.transform;
    }

    private void OnEnable()
    {
        _inputReader.MoveEvent += OnMove;
    }
    private void OnDisable()
    {
        _inputReader.MoveEvent -= OnMove;
    }

    void Update()
    {
        _groundedPlayer = _controller.isGrounded;
        if (_groundedPlayer && _playerVelocity.y < 0)
        {
            _playerVelocity.y = 0f;
        }
        Vector3 move = new Vector3(_movement.x, 0f, _movement.y);
        move = _cameraTransform.forward * move.z + _cameraTransform.right * move.x;
        move.y = 0f;
        _controller.Move(move * Time.deltaTime * _playerSpeed);

        _playerVelocity.y += _gravityValue * Time.deltaTime;
        _controller.Move(_playerVelocity * Time.deltaTime);
    }

    private void OnMove(Vector2 movement)
    {
        _movement = movement;
    }
}
