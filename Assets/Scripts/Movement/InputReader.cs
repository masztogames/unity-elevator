using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "InputReader", menuName = "Game/Input Reader")]
public class InputReader : ScriptableObject, FPPInputActions.IPlayerActions
{
    public event UnityAction InteractionButtonEvent = delegate { };
    public event UnityAction<Vector2> MoveEvent = delegate { };
    public event UnityAction<Vector2> CameraMoveEvent = delegate { };

    private FPPInputActions _gameInput;

    private void OnEnable()
    {
        if (_gameInput == null)
        {
            _gameInput = new FPPInputActions();

            _gameInput.Player.SetCallbacks(this);
        }
    }

    public void OnMove(InputAction.CallbackContext context)
    {
        MoveEvent.Invoke(context.ReadValue<Vector2>());
    }

    public void OnLook(InputAction.CallbackContext context)
    {
        CameraMoveEvent.Invoke(context.ReadValue<Vector2>());
    }

    public void OnInteraction(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Performed)
        {
            InteractionButtonEvent.Invoke();
        }
    }
}