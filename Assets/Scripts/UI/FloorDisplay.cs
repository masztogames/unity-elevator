using UnityEngine;
using TMPro;

public class FloorDisplay : MonoBehaviour
{
    public IntVariable CurrentFloor;
    public ElevatorStateSO State;
    [SerializeField] private GameObject _moveArrows;

    private TextMeshProUGUI _displayText;

    private void Start()
    {
        _displayText = GetComponent<TextMeshProUGUI>();
    }
    void Update()
    {
        _displayText.text = CurrentFloor.Value.ToString();
        _moveArrows.SetActive(State.CurrentElevatorState == ElevatorState.Moving ? true : false);
    }
}
