using UnityEngine;

public class CloseDoorButton : MonoBehaviour, IInteractable
{
    public float MaxRange { get { return _maxRange; } }
    private const float _maxRange = 1f;

    [SerializeField] private string _buttonText;

    [SerializeField] private AudioClip _buttonClick;

    private AudioSource _audioSource;

    public Elevator Elevator;

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }
    public  void OnInteract()
    {
        _audioSource.PlayOneShot(_buttonClick);
        Elevator.CloseDoorButton();
    }
}