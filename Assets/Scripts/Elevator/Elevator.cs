using UnityEngine;

public class Elevator : MonoBehaviour
{
    public bool IsElevatorCalled;

    public DoorsHandler DoorsHandler;

    public IntVariable FloorNumber;
    [SerializeField] private ElevatorStateSO _elevatorState;

    [SerializeField] private float _elevatorSpeed;

    private int _lastFloorNumber;
    private Transform _newPosition;

    void Start()
    {
        _lastFloorNumber = FloorNumber.Value;
    }

    void FixedUpdate()
    {
        if (IsElevatorCalled)
        {
            MoveElevator();
        }

    }
    public void SetNewTargetPosition(Transform newPosition)
    {
        _newPosition = newPosition;
        IsElevatorCalled = true;
    }
    private void MoveElevator()
    {
        DoorsHandler.CloseDoorsOnFloor(_lastFloorNumber);

        if (IsElevatorOnPosition())
        {
            return;
        }
        if (_elevatorState.CurrentElevatorState == ElevatorState.Blocked)
        {
            return;
        }
        _elevatorState.UpdateElevatorState(ElevatorState.Moving);
        transform.position = Vector3.Lerp(transform.position, _newPosition.position, _elevatorSpeed);
    }
    private bool IsElevatorOnPosition()
    {
        if (transform.position == _newPosition.position)
        {
            _elevatorState.UpdateElevatorState(ElevatorState.Blocked);
            DoorsHandler.OpenDoorsOnFloor(FloorNumber.Value);

            IsElevatorCalled = false;
            return true;
        }
        return false;
    }
    public void OpenDoorButton()
    {
        DoorsHandler.OpenDoorsOnFloor(FloorNumber.Value);
    }
    public void CloseDoorButton()
    {
        DoorsHandler.CloseDoorsOnFloor(FloorNumber.Value);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Player")
        {
            other.transform.parent = this.transform;
        }
    }
    private void OnTriggerExit(Collider other)
    {

        if (other.transform.tag == "Player")
        {
            other.transform.parent = null;
        }
    }
}




