using System.Collections;
using UnityEngine;

public class ElevatorHandler : MonoBehaviour
{
    public Queue ElevatorQueue;
    public Elevator Elevator;
    public ElevatorStateSO State;

    public IntVariable FloorNumber;

    public Transform[] Floors;

    void Start()
    {
        ElevatorQueue = new Queue();
    }
    private void OnEnable()
    {
        ElevatorCallButton.OnCallElevator += AddCallToQueue;
    }
    private void OnDisable()
    {
        ElevatorCallButton.OnCallElevator -= AddCallToQueue;
    }
    public void AddCallToQueue(int floorNumber)
    {
        ElevatorQueue.Enqueue(floorNumber);

    }
    private void CallElevator()
    {
        int nextDequeuePosition = (int)ElevatorQueue.Dequeue();
        FloorNumber.Value = nextDequeuePosition;

        Elevator.SetNewTargetPosition(Floors[nextDequeuePosition]);
    }
    private bool IsElevatorFree()
    {
        return State.CurrentElevatorState == ElevatorState.CanMove ? true : false;
    }
    void Update()
    {
        if (ElevatorQueue.Count > 0)
        {
            for (int i = 0; i < ElevatorQueue.Count; i++)
            {
                if (IsElevatorFree())
                {
                    CallElevator();
                }

            }
        }
    }
}
