using UnityEngine;

public class ElevatorCallButton : MonoBehaviour, IInteractable
{
    public float MaxRange { get { return _maxRange; } }
    private const float _maxRange = 1f;

    [SerializeField] private string _buttonText;
    [SerializeField] private int _floorNumber;

    [SerializeField] private AudioClip _buttonClick;

    private AudioSource _audioSource;

    public IntVariable FloorNumber;

    public delegate void CallElevator(int value);
    public static event CallElevator OnCallElevator;

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }
    public void OnInteract()
    {
        _audioSource.PlayOneShot(_buttonClick);
        OnCallElevator(_floorNumber);
    }
}
