# Unity Elevator

Fully functional elevator system in Unity 3d.

## Screenshots

![Call the elevator](/Assets/Screens/1.png)

![Elevator Inside](/Assets/Screens/3.png)

![Automatic doors](/Assets/Screens/4.png)

## What's included?

The project consis of:

- Player movement using new input system and cinemachine.

- Elevator system with the function of calling the elevator, intelligent opening / closing doors, queuing selected floors.

- An exemplary scene with a presentation of the entire system.

## Requirements

Necessary to be implemented in your project are:
- Unity ver. 2020.2.6f1

- [New Input System](https://docs.unity3d.com/Packages/com.unity.inputsystem@1.0/manual/QuickStartGuide.html)

- [Cinemaschine](https://unity.com/unity/features/editor/art-and-design/cinemachine)

## Audio

- [ ] [Local Forecast - Elevator](https://www.youtube.com/watch?v=FgXYzF5-Yiw) · [@Kevin MacLeod](https://www.youtube.com/channel/UCSZXFhRIx6b0dFX3xS8L1yQ)


## Author

@masztogames Tomasz Janicki

## License

Open source code. It would be nice if you would give me some feedback.

If you use my code in your game, let me know :).
